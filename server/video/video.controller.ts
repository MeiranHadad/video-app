import { Request, Response, NextFunction } from 'express';
import * as fs from 'fs';
import { Video } from './video.interface';
import { VideoManager } from './video.manager';

export class VideoController {

    static async upload(req: Request, res: Response) {
        const {name , description} = req.query as unknown as Video;

        res.json(await VideoManager.upload(req.files, {name, date: new Date(), description}));
    }

    static async getMany(req: Request, res: Response) {
        type QueryType = {
            startIndex: number;
            endIndex: number;
            name?: string;
            date?: Date;
            description?: string;
        };

        const query = req.query as unknown as QueryType & Partial<Video>;

        const {startIndex, endIndex, ...videoFields} = query;

        res.json(await VideoManager.getMany(startIndex, endIndex, videoFields));
    }

    static download(req: Request, res: Response) {
        
        const path = `./publicVideos/${req.params.id}`
        const stat = fs.statSync(path)
        const fileSize = stat.size
        const range = req.headers.range
        
        // if (range) {
        // const parts = range.replace(/bytes=/, "").split("-")
        // const start = parseInt(parts[0], 10)
        // const end = parts[1]
        // ? parseInt(parts[1], 10)
        // : fileSize-1
        
        // const chunksize = (end-start)+1
        // const file = fs.createReadStream(path, {start, end})
        // const head = {
        // 'Content-Range': `bytes ${start}-${end}/${fileSize}`,
        // 'Accept-Ranges': 'bytes',
        // 'Content-Length': chunksize,
        // 'Content-Type': 'video/mp4',
        // }
        
        // res.writeHead(206, head)
        // file.pipe(res)
        // } else {
        const head = {
        'Content-Length': fileSize,
        'Content-Type': 'video/mp4',
        }
        res.writeHead(200, head)
        fs.createReadStream(path).pipe(res)
        // }
    }

}