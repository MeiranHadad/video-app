import { Video } from './video.interface';
import { VideoModel } from './video.model';

export class VideoRepository {

    static create(video: Video) {
       return VideoModel.create(video);
    }

    static getMany(startIndex: number, endIndex: number,videoFields: Partial<Video>) {

        return VideoModel.find({
            $and: [videoFields],
        })
        .skip(startIndex)
        .limit(endIndex - startIndex)
        .exec();
    }

}