import { Router } from 'express';
import { VideoController } from './video.controller';

const VideoRouter: Router = Router();

VideoRouter.post('/upload', VideoController.upload);

VideoRouter.get('/download/:id', VideoController.download);

VideoRouter.get('/', VideoController.getMany);

export { VideoRouter };
