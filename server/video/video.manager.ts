import * as fs from 'fs';
import { Video } from './video.interface';
import { VideoRepository } from './video.repository';

export class VideoManager {

    static async upload(files: any, video: Video) {
        const folderPath =  './publicVideos';

        if (!files) {
            return("File was not found");
        } 
        const file: any = files.file;
        const uploadPath = `./publicVideos/${file.name}`;

        if (!fs.existsSync(folderPath)){
            fs.mkdirSync(folderPath);
        }

        file.mv(uploadPath, (err: any) => {
            if (err)
            return err;
    
        });

        const videoToCreate = await VideoRepository.create(video);

        return videoToCreate;
    }

    static async getMany(startIndex: number, endIndex: number, videoFields: Partial<Video>) {
        return VideoRepository.getMany( startIndex, endIndex, videoFields);
    }

}