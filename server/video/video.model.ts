import * as mongoose from 'mongoose';
import { Video } from './video.interface';

export const VideoSchema: mongoose.Schema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        date: {
            type: Date,
            required: false,
        },
        description: {
            type: String,
            required: false,
        }
    }
)

export const VideoModel = mongoose.model<Video & mongoose.Document>('Video', VideoSchema);