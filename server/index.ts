import * as mongoose from 'mongoose';
import { Server } from './server';
import { config } from './config';

(async () => {
  mongoose.connection.on('connecting', () => {
    console.log('[MongoDB] connecting...');
  });

  mongoose.connection.on('connected', () => {
    console.log('[MongoDB] connected');
  });

  
  await mongoose.connect(config.db.connectionURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  Server.startServer();

})();