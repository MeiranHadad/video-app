import { Router } from 'express';
import { VideoRouter } from './video/video.router';

const AppRouter: Router = Router();

AppRouter.use('/api/video', VideoRouter);


AppRouter.get('/isAlive', (req, res) => {
  res.status(200).send('alive');
});

AppRouter.use('*', (req, res) => {
  res.status(404).send('Invalid Route');
});

export { AppRouter };
