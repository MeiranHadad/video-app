import * as express from 'express';
import { AppRouter } from './router';
import { config } from './config';
import * as  fileUpload  from 'express-fileupload';

export class Server {
    public app: express.Application;

    private static serverInstance: Server;

    public static startServer(): Server {
        if (!this.serverInstance) {
          this.serverInstance = new Server();
          return this.serverInstance;
        }
        return this.serverInstance;
      }


    private constructor() {
        this.app = express();
        this.app.use(this.setHeaders);
        this.app.use(fileUpload());
        this.app.use(AppRouter);
        this.app.listen(config.server.port, () => {
          console.log(`listening on port ${config.server.port}`,
          );
        });
    }

    private setHeaders = (req: express.Request, res: express.Response,
        next: express.NextFunction) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Credentials', 'true');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        res.setHeader('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, Content-Type');
        next();
    };
    
}