"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const server_1 = require("./server");
const config_1 = require("./config");
(async () => {
    mongoose.connection.on('connecting', () => {
        console.log('[MongoDB] connecting...');
    });
    mongoose.connection.on('connected', () => {
        console.log('[MongoDB] connected');
    });
    await mongoose.connect(config_1.config.db.connectionURL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
    });
    server_1.Server.startServer();
})();
//# sourceMappingURL=index.js.map