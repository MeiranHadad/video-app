"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadRouter = void 0;
const express_1 = require("express");
const upload_controller_1 = require("./upload.controller");
const UploadRouter = (0, express_1.Router)();
exports.UploadRouter = UploadRouter;
UploadRouter.post('/', upload_controller_1.UploadController.upload);
//# sourceMappingURL=upload.router.js.map