"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadController = void 0;
const fs = require("fs");
class UploadController {
    static upload(req, res, next) {
        const folderPath = './publicVideos';
        if (!req.files) {
            res.send("File was not found");
            return;
        }
        const file = req.files.file;
        const uploadPath = `./publicVideos/${file.name}`;
        if (!fs.existsSync(folderPath)) {
            fs.mkdirSync(folderPath);
        }
        file.mv(uploadPath, (err) => {
            if (err)
                return res.status(500).send(err);
        });
        res.json(file.name);
    }
}
exports.UploadController = UploadController;
//# sourceMappingURL=upload.controller.js.map