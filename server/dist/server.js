"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Server = void 0;
const express = require("express");
const router_1 = require("./router");
const config_1 = require("./config");
const fileUpload = require("express-fileupload");
class Server {
    constructor() {
        this.setHeaders = (req, res, next) => {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Credentials', 'true');
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
            res.setHeader('Access-Control-Allow-Headers', 'Authorization, Origin, X-Requested-With, Content-Type');
            next();
        };
        this.app = express();
        this.app.use(this.setHeaders);
        this.app.use(fileUpload());
        this.app.use(router_1.AppRouter);
        this.app.listen(config_1.config.server.port, () => {
            console.log(`listening on port ${config_1.config.server.port}`);
        });
    }
    static startServer() {
        if (!this.serverInstance) {
            this.serverInstance = new Server();
            return this.serverInstance;
        }
        return this.serverInstance;
    }
}
exports.Server = Server;
//# sourceMappingURL=server.js.map