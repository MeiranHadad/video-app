"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppRouter = void 0;
const express_1 = require("express");
const video_router_1 = require("./video/video.router");
const AppRouter = (0, express_1.Router)();
exports.AppRouter = AppRouter;
AppRouter.use('/api/video', video_router_1.VideoRouter);
AppRouter.get('/isAlive', (req, res) => {
    res.status(200).send('alive');
});
AppRouter.use('*', (req, res) => {
    res.status(404).send('Invalid Route');
});
//# sourceMappingURL=router.js.map