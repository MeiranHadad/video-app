"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DownloadRouter = void 0;
const express_1 = require("express");
const download_controller_1 = require("./download.controller");
const DownloadRouter = (0, express_1.Router)();
exports.DownloadRouter = DownloadRouter;
DownloadRouter.get('/:id', download_controller_1.DownloadController.download);
//# sourceMappingURL=download.router.js.map