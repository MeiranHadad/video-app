"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VideoManager = void 0;
const fs = require("fs");
const video_repository_1 = require("./video.repository");
class VideoManager {
    static async upload(files, video) {
        const folderPath = './publicVideos';
        if (!files) {
            return ("File was not found");
        }
        const file = files.file;
        const uploadPath = `./publicVideos/${file.name}`;
        if (!fs.existsSync(folderPath)) {
            fs.mkdirSync(folderPath);
        }
        file.mv(uploadPath, (err) => {
            if (err)
                return err;
        });
        const videoToCreate = await video_repository_1.VideoRepository.create(video);
        return videoToCreate;
    }
    static async getMany(startIndex, endIndex, videoFields) {
        return video_repository_1.VideoRepository.getMany(startIndex, endIndex, videoFields);
    }
}
exports.VideoManager = VideoManager;
//# sourceMappingURL=video.manager.js.map