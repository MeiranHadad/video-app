"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VideoModel = exports.VideoSchema = void 0;
const mongoose = require("mongoose");
exports.VideoSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    date: {
        type: Date,
        required: false,
    },
    description: {
        type: String,
        required: false,
    }
});
exports.VideoModel = mongoose.model('Video', exports.VideoSchema);
//# sourceMappingURL=video.model.js.map