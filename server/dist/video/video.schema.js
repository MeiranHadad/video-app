"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.videoSchema = void 0;
const Joi = require("joi");
exports.videoSchema = Joi.object({
    body: {
        id: Joi.string(),
        name: Joi.string().required(),
        date: Joi.date().default(new Date()),
        description: Joi.string(),
    },
    params: {},
    query: {},
});
//# sourceMappingURL=video.schema.js.map