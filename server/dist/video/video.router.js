"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VideoRouter = void 0;
const express_1 = require("express");
const video_controller_1 = require("./video.controller");
const VideoRouter = (0, express_1.Router)();
exports.VideoRouter = VideoRouter;
VideoRouter.post('/upload', video_controller_1.VideoController.upload);
VideoRouter.get('/download/:id', video_controller_1.VideoController.download);
VideoRouter.get('/', video_controller_1.VideoController.getMany);
//# sourceMappingURL=video.router.js.map