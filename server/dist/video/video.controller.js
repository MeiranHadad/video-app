"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VideoController = void 0;
const fs = require("fs");
const video_manager_1 = require("./video.manager");
class VideoController {
    static async upload(req, res) {
        const { name, description } = req.query;
        res.json(await video_manager_1.VideoManager.upload(req.files, { name, date: new Date(), description }));
    }
    static async getMany(req, res) {
        const query = req.query;
        const { startIndex, endIndex, ...videoFields } = query;
        res.json(await video_manager_1.VideoManager.getMany(startIndex, endIndex, videoFields));
    }
    static download(req, res) {
        const path = `./publicVideos/${req.params.id}`;
        const stat = fs.statSync(path);
        const fileSize = stat.size;
        const range = req.headers.range;
        // if (range) {
        // const parts = range.replace(/bytes=/, "").split("-")
        // const start = parseInt(parts[0], 10)
        // const end = parts[1]
        // ? parseInt(parts[1], 10)
        // : fileSize-1
        // const chunksize = (end-start)+1
        // const file = fs.createReadStream(path, {start, end})
        // const head = {
        // 'Content-Range': `bytes ${start}-${end}/${fileSize}`,
        // 'Accept-Ranges': 'bytes',
        // 'Content-Length': chunksize,
        // 'Content-Type': 'video/mp4',
        // }
        // res.writeHead(206, head)
        // file.pipe(res)
        // } else {
        const head = {
            'Content-Length': fileSize,
            'Content-Type': 'video/mp4',
        };
        res.writeHead(200, head);
        fs.createReadStream(path).pipe(res);
        // }
    }
}
exports.VideoController = VideoController;
//# sourceMappingURL=video.controller.js.map