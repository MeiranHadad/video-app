"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VideoRepository = void 0;
const video_model_1 = require("./video.model");
class VideoRepository {
    static create(video) {
        return video_model_1.VideoModel.create(video);
    }
    static getMany(startIndex, endIndex, videoFields) {
        return video_model_1.VideoModel.find({
            $and: [videoFields],
        })
            .skip(startIndex)
            .limit(endIndex - startIndex)
            .exec();
    }
}
exports.VideoRepository = VideoRepository;
//# sourceMappingURL=video.repository.js.map