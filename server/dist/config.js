"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = void 0;
exports.config = {
    server: {
        port: 3000
    },
    db: {
        connectionURL: 'mongodb://localhost:27017/videoApp',
    },
};
//# sourceMappingURL=config.js.map