export const config = {
    server: {
        port: 3000
    },
    db: {
        connectionURL: 'mongodb://localhost:27017/videoApp',
    },
}