import { Component, OnInit } from '@angular/core';
import { VideoService } from 'src/app/services/video.service';
import { Video } from 'src/app/shared/video.interface';

@Component({
  templateUrl: './download.page.html',
  styleUrls: ['./download.page.css']
})
export class DownloadPage implements OnInit {

  videos!: Video[];

  constructor(private videoService: VideoService) {
  }

  ngOnInit(): void {
    this.loadVideosUrls();
  }

  loadVideosUrls(): void {
    this.videoService.getMany(0, 5).subscribe((videos) => {
      this.videos = videos;
    });
  }


  downloadVideoByUrl(videoName: string): string {
    return `http://localhost:3000/api/video/download/${videoName}`;
  }

}
