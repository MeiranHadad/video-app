import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DownloadPage } from './pages/download/download.page';



@NgModule({
  declarations: [DownloadPage],
  imports: [
    CommonModule
  ],
  exports: [DownloadPage],
})
export class DownloadModule { }
