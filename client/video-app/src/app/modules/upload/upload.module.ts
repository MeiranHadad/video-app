import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadPage } from './pages/upload/upload.page';



@NgModule({
  declarations: [UploadPage],
  imports: [
    CommonModule
  ],
  exports: [UploadPage],
})
export class UploadModule { }
