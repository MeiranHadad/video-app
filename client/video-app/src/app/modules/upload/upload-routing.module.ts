import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UploadPage } from './pages/upload/upload.page';

const routes: Routes = []

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class UploadRoutingModule {}