import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { VideoService } from 'src/app/services/video.service';

@Component({
  templateUrl: './upload.page.html',
  styleUrls: ['./upload.page.css']
})
export class UploadPage {

  fileToUpload: File | null = null;

  constructor(private videoService: VideoService) { }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  onFileSelected(changeEvent: Event, description: string) {
    const inputData = changeEvent.target as HTMLInputElement;

    const fileToUpload = inputData.files?.item(0) as File;

    this.videoService.upload(fileToUpload, description).subscribe();

  }

}
