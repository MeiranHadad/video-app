export interface Video {
    name: string;
    date: Date;
    description: string;
}