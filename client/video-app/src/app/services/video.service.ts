import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Video } from '../shared/video.interface';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  filesSubject = new Subject<any>();

  serviceUrl = 'http://localhost:3000/api/video';

  constructor(private httpClient: HttpClient) { }

  upload(videoToUpload: File, description?: string): Observable<Video> {
    let params: HttpParams = new HttpParams();
    
    const formData: FormData = new FormData();

    formData.append('file', videoToUpload);
    params = params.append('name', videoToUpload.name);
    params = params.append('description', description || ' ');

    return this.httpClient.post<Video>(`${this.serviceUrl}/upload` ,formData, {params});
  }

  download(videoName: string): Observable<Video> {
    return this.httpClient.get<Video>(`${this.serviceUrl}/download/${videoName}`);
  }

  getMany(startIndex?: number, endIndex?: number): Observable<Video[]> {
    let params: HttpParams = new HttpParams();

    params = params.append('startIndex', startIndex as unknown as any || 0);
    params = params.append('endIndex', endIndex as unknown as any || 20);

    return this.httpClient.get<Video[]>(`${this.serviceUrl}/`);
  }
}
