import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DownloadPage } from './modules/download/pages/download/download.page';
import { UploadPage } from './modules/upload/pages/upload/upload.page';

const routes: Routes = [
  {
    path: 'upload',
    component: UploadPage,
  },
  {
    path: 'download',
    component: DownloadPage,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
